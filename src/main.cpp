using namespace std;

#include "main.h"

MYSQL db;
MYSQL_RES *res;
MYSQL_ROW row;

void ClientThread(SOCKET csocket);
void InsimTread(SERVERS pack_serv);
void ServersCheckerThread();

/** client functions **/



void irp_hlr(IsRelay *client)
{
    struct IR_HLR *pack_nlr = (struct IR_HLR*)client->get_packet();


    IR_HOS pack_hos;
    memset(&pack_hos,0,sizeof(IR_HOS));
    pack_hos.Size = sizeof(IR_HOS);
    pack_hos.Type = IRP_HOS;
    pack_hos.ReqI = pack_nlr->ReqI;

    unsigned int i = 0;
    unsigned int g = 0;

    if(servers.size() > 0)
    {
        for (auto server: servers)
        {
            i++;
            int id = server.first;

            strcpy(pack_hos.Info[g%6].HName,servers[id].HName.c_str());
            strcpy(pack_hos.Info[g%6].Track,servers[id].Track.c_str());
            pack_hos.Info[g%6].NumConns = servers[id].NumConns;

            pack_hos.Info[g%6].Flags = servers[id].Flags;

            if (i == 1)
                pack_hos.Info[g].Flags += HOS_FIRST;

            if (i == servers.size())
                 pack_hos.Info[g].Flags += HOS_LAST;


            g++;

            if (g > 0 && g%6 == 0)
            {
                pack_hos.NumHosts = g;
                client->send_packet(&pack_hos);


                memset(pack_hos.Info,0,sizeof(HInfo)*6);

                g = 0;
            }
        }
    }

    if (g > 0)
    {
        pack_hos.NumHosts = g;
        client->send_packet(&pack_hos);
    }
}
int irp_sel(IsRelay *client)
{
    struct IR_SEL *pack_sel = (struct IR_SEL*)client->get_packet();

    if(servers.size() > 0)
    {
        for (auto server: servers)
        {
            int i = server.first;

            if (servers[i].HName == string(pack_sel->HName))
            {
                // <!--check specpass-->
                if (servers[i].Flags&HOS_SPECPASS)
                {
                    // if Spectator pass required, but none given
                    if (strcmp(pack_sel->Spec,"") == 0)
                    {
                        struct IR_ERR error;
                        memset(&error,0,sizeof(struct IR_ERR));
                        error.Size=sizeof(struct IR_ERR);		// 4
                        error.Type= IRP_ERR;		// IRP_ERR
                        error.ReqI = client->get_reqi();		// As given in RL_SEL, otherwise 0
                        error.ErrNo = IR_ERR_NOSPEC;		// Error number - see NOTE 2) below
                        client->send_packet(&error);
                        return 0;
                    }
                    // if Wrong spec pass given by client
                    else if (servers[i].Spec == string(pack_sel->Spec))
                    {
                        struct IR_ERR error;
                        memset(&error,0,sizeof(struct IR_ERR));
                        error.Size=sizeof(struct IR_ERR);		// 4
                        error.Type= IRP_ERR;		// IRP_ERR
                        error.ReqI = client->get_reqi();		// As given in RL_SEL, otherwise 0
                        error.ErrNo = IR_ERR_SPEC;		// Error number - see NOTE 2) below
                        client->send_packet(&error);
                        return 0;
                    }
                }

                if (strlen(pack_sel->Admin) > 0)
                {
                    // if admin > 0 and it's true pass
                    if (servers[i].Admin != string(pack_sel->Admin))
                    {
                        struct IR_ERR error;
                        memset(&error,0,sizeof(struct IR_ERR));
                        error.Size=sizeof(struct IR_ERR);		// 4
                        error.Type= IRP_ERR;		// IRP_ERR
                        error.ReqI = client->get_reqi();		// As given in RL_SEL, otherwise 0
                        error.ErrNo = IR_ERR_ADMIN;		// Error number - see NOTE 2) below
                        client->send_packet(&error);
                        return 0;
                    }
                    else
                    {
                        client->Admin = 1;
                    }

                }
                else
                {
                    client->Admin = 0;
                }

                // <--check specpass-->

                clients[client->sock] = i;

                servers[i].insim->SendTiny(TINY_VER,client->get_reqi());

                printf("Client select %s\n",pack_sel->HName);

                break;
            } // HName == HName
        }
    }

    return 0;
}




void irp_arq(IsRelay *client)
{
    IS_TINY pack_ok;
    memset(&pack_ok,0,sizeof(IS_TINY));
    pack_ok.Size = sizeof(IS_TINY);
    pack_ok.Type = IRP_ARP;
    pack_ok.ReqI = 1;
    pack_ok.SubT = client->Admin;
    client->send_packet(&pack_ok);
}

void irp_close(IsRelay *client)
{
    struct IR_CLOSE *pack_cls = (struct IR_CLOSE*)client->get_packet();

    for (auto s: servers)
    {
        if (s.first == (int)pack_cls->ServID )
        {
            servers[s.first].ok = 0;
            break;
        }
    }
}


/** end of client functions **/


int main(int argc, char* argv[])
{
    if (!mysql_init(&db)){
        printf("Can't creat MySQL descriptor\n");
        return 0;
    }

    while ( !mysql_real_connect(&db, MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB, MYSQL_PORT, NULL, 0) ){
        printf("Can't connect to MySQL server ( %s )\n",mysql_error(&db));

        sleep(60);
    }

    printf("Connected to mysql server\n");

    mysql_query(&db,"UPDATE servers SET status='0' WHERE NOT status IN ('2');");

    thread(ServersCheckerThread).detach();

    #ifdef _WINDOWS
    WSADATA wsaData;
    if (WSAStartup(0x0202,(WSADATA *) &wsaData))
    {
        // Ошибка!
        printf("Error WSAStartup %d\n",WSAGetLastError());
        return -1;
    }
	#endif
    // printf("Шаг 2 - создание сокета\n");
    SOCKET mysocket;
    // AF_INET     - сокет Интернета
    // SOCK_STREAM  - потоковый сокет (с
    //      установкой соединения)
    // 0      - по умолчанию выбирается TCP протокол
    if ((mysocket=socket(AF_INET,SOCK_STREAM,0))<0)
    {
        // Ошибка!
        #ifdef _WINDOWS
        printf("Error socket %d\n",WSAGetLastError());
        WSACleanup();
        #endif
        // Деиницилизация библиотеки Winsock
        return -1;
    }

    // Шаг 3 связывание сокета с локальным адресом
    sockaddr_in local_addr;
    local_addr.sin_family=AF_INET;
    local_addr.sin_port=htons(MY_PORT);
    // не забываем о сетевом порядке!!!
    local_addr.sin_addr.s_addr=0;
    // сервер принимает подключения
    // на все IP-адреса

    // printf( "Вызываем bind для связывания\n");
    if (bind(mysocket,(sockaddr *)&local_addr,sizeof(local_addr)))
    {
        // Ошибка
        #ifdef _WINDOWS
        printf("Error bind %d\n",WSAGetLastError());
        closesocket(mysocket);  // закрываем сокет!
        WSACleanup();
        #else
        close(mysocket);
        #endif
        return -1;
    }

    // Шаг 4 ожидание подключений
    // размер очереди – 0x100
    if (listen(mysocket, 0x100))
    {
        // Ошибка
        #ifdef _WINDOWS
        printf("Error listen %d\n",WSAGetLastError());
        closesocket(mysocket);
        WSACleanup();
        #else
        close(mysocket);
        #endif
        return -1;
    }
    printf("Create Server on *:%d\n", MY_PORT);
    printf("Wait for connect\n");

    // Шаг 5 извлекаем сообщение из очереди
    SOCKET client_socket;    // сокет для клиента
    sockaddr_in client_addr;    // адрес клиента
    // (заполняется системой)

    // функции accept необходимо передать размер
    // структуры
    #ifdef _WINDOWS
    int client_addr_size=sizeof(client_addr);
    #else
    socklen_t client_addr_size=sizeof(client_addr);
    #endif // _WINDOWS

    // цикл извлечения запросов на подключение из
    // очереди

    while ((client_socket=accept(mysocket, (sockaddr*)&client_addr, &client_addr_size)))
    {

        printf("Client connected\n");
        //printf("new client!\n");
		thread(ClientThread,client_socket).detach();
    }

    mysql_close(&db);
    #ifdef _WINDOWS
    closesocket(mysocket);
    WSACleanup();
    #else
    close(mysocket);
    #endif

    return 0;
}


void ClientThread(SOCKET client_socket)
{

    IsRelay client;

    client.sock = client_socket;

    int ok = 1;

    while ( ok > 0)
    {


        if (client.next_packet() < 0)
        {
            break;
        }

        /** POLICY **/

        char get[IS_PACKET_MAX_SIZE];

        memcpy(get,client.get_packet(),client.packet_size);


        if ( strstr( get,"<policy-file-request/>" ) != NULL )
        {

            printf("Request Policy\n");

            string request;
            request = "<?xml version=\"1.0\"?>";
            request += "<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">";
            request += "<cross-domain-policy>";
            request += "<site-control permitted-cross-domain-policies=\"master-only\" />";
            request += "<allow-access-from domain=\"*\" to-ports=\"29340\" />";
            request += "<allow-access-from domain=\"*\" to-ports=\"29340\" />";
            request += "</cross-domain-policy>";
            request += "\x0A\x00";

            client.send_packet( request.c_str() , request.length() +1 );
            break;
        }

        /** **/
        if ((client.peek_packet() == 3) and (client.type_packet() == -1))
        {
            printf("Request Version\n");
            struct IR_VER pack;
            memset(&pack,0,sizeof(struct IR_VER));
            pack.Size = sizeof(struct IR_VER);
            pack.Type = 4;
            pack.ReqI = client.get_reqi();
            pack.ReqI2 = client.type_packet();
            pack.Ver = 213;
            client.send_packet(&pack);
            //continue;
        }

        switch ((unsigned)(client.peek_packet()))
        {
        case IRP_CLOSE:
            irp_close(&client);
            ok = 0;
            break;

        case IRP_HLR:
            irp_hlr(&client);
            break;

        case IRP_SEL:
            irp_sel(&client);
            break;

        case IRP_ARQ:
            irp_arq(&client);
            break;

            /**************/
            default:
                if(clients.find(client_socket) != clients.end())
                {
                    servers[ clients[client_socket] ].insim->send_packet(client.get_packet());
                }
            break;

        }// switch

    } // for (;;)

    clients.erase(client_socket);

	#ifdef _WINDOWS
    closesocket(client_socket);
    #else
    close(client_socket);
    #endif

    printf("Client disconnected\n");
    return;
}


void InsimTread(SERVERS pack_serv)
{

    char query[64];

    CInsim *insim;
    insim = new CInsim();

    int serv_id = atoi(pack_serv.id);

    struct IS_VER pack_ver;

    printf("Try connect to: %s:%d\n",pack_serv.IP,pack_serv.Port);

    int ConnCount = 0;

    while (ConnCount < 5)
	{
        if ( insim->init (pack_serv.IP, pack_serv.Port, IS_PRODUCT_NAME,pack_serv.AdminPass, &pack_ver, '!', ISF_MCI+ISF_NLP,500,0) < 0 )
        {
            sprintf(query,"UPDATE isrelay SET status='2' WHERE id='%d';",serv_id);
            mysql_query(&db,query);

            sleep(60);

            ++ConnCount;
        }
        else{
            ConnCount = 0;
            break;
        }

    }

    if(ConnCount >= 5)
        return;

    printf("Connected to: %s:%d\n",pack_serv.IP,pack_serv.Port);


    servers[serv_id].ID = serv_id;

    sprintf(query,"UPDATE isrelay SET status='1' WHERE id='%d';",serv_id);
    mysql_query(&db,query);

    servers[serv_id].Flags = HOS_LICENSED;

    if (strcmp(pack_ver.Product,"S1") == 0 )
        servers[serv_id].Flags += HOS_S1;

    if (strcmp(pack_ver.Product,"S2") == 0 )
        servers[serv_id].Flags += HOS_S2;

    if (strcmp(pack_ver.Product,"S3") == 0 )
        servers[serv_id].Flags += HOS_S3;

    servers[serv_id].insim = insim;

    servers[serv_id].Admin = pack_serv.AdminPass;

    if (strlen(pack_serv.SpecPass) > 0)
    {
        servers[serv_id].Flags += HOS_SPECPASS;
        servers[serv_id].Spec = pack_serv.SpecPass;
    }

    insim->SendTiny(TINY_ISM,198);
    insim->SendTiny(TINY_RST,198);
    insim->SendTiny(TINY_SST,198);

    servers[serv_id].ok = 1;
    while (servers[serv_id].ok > 0)
    {
        if (insim->next_packet() < 0)
        {
            printf("%s:%d - Error get next paket",pack_serv.IP,pack_serv.Port);
            insim->isclose();
            ConnCount = 0;
            while (ConnCount < 5){
                if (insim->init (pack_serv.IP, pack_serv.Port, IS_PRODUCT_NAME,pack_serv.AdminPass, &pack_ver, '!', ISF_MCI+ISF_NLP,500,0) < 0)
                {
                    sprintf(query,"UPDATE isrelay SET status='2' WHERE id='%d';",serv_id);
                    mysql_query(&db,query);

                    sleep(60);

                    ++ConnCount;
                }
                else{
                    ConnCount = 0;
                    break;
                }

            }

            break;
        }

        if (insim->peek_packet() == ISP_ISM)
        {
            struct IS_ISM *pack_ism = (struct IS_ISM*)insim->get_packet();

            string HostName = pack_ism->HName;
            size_t pos;

            while ((pos = HostName.find("^")) != string::npos )
            {
                HostName.replace(pos,2,"");
            }

            servers[serv_id].HName =  HostName;
            sprintf(query,"UPDATE isrelay SET name='%s' WHERE id='%d';",pack_ism->HName,serv_id);
            mysql_query(&db,query);
        }




        if (insim->peek_packet() == ISP_RST)
        {
            struct IS_RST *pack_rst = (struct IS_RST*)insim->get_packet();
            servers[serv_id].Track = pack_rst->Track;
        }


        if (insim->peek_packet() == ISP_STA)
        {
            struct IS_STA *pack_sta = (struct IS_STA*)insim->get_packet();
            servers[serv_id].NumP = pack_sta->NumP;
            servers[serv_id].NumConns = pack_sta->NumConns;
        }

        if(clients.size() > 0)
        {
            for (auto client: clients)
            {
                if (serv_id == client.second)
                {
                    send(client.first, (const char *)insim->get_packet(), *((unsigned char*)insim->get_packet()), 0);
                }
            }
        }

    }

    servers[serv_id].NumConns = 0;
    servers[serv_id].ID = 0;


    sprintf(query,"UPDATE isrelay SET status='2' WHERE id='%d';",serv_id);
    mysql_query(&db,query);

    printf("Close connection with: %s:%d",pack_serv.IP,pack_serv.Port);

    if (insim->isclose() < 0)
    {
        //cerr << "Error closing connection" << endl;
        return;
    }
    return;
}

void ServersCheckerThread()
{
    for (;;)
    {
        mysql_query(&db,"SELECT id,ip,port,admin,spec FROM isrelay WHERE status IN ('0') ORDER BY id;");
        res = mysql_store_result(&db);
        if (res != NULL and mysql_num_rows(res) > 0){

            while ((row = mysql_fetch_row(res)) != NULL){

                struct SERVERS Server;
                memset(&Server,0,sizeof(SERVERS));

                strncpy(Server.id,row[0],5);

                strncpy(Server.IP,row[1],16);

                Server.Port = atoi(row[2]);

                strncpy(Server.AdminPass,row[3],24);

                strncpy(Server.SpecPass,row[4],24);

                thread(InsimTread, Server).detach();

                sleep(2);

            }

            mysql_free_result(res);
        }

        sleep(60);
    }

    return;
}
