#include "IsRelay.h"

IsRelay::IsRelay ()
{
    // Initialize packet buffers
    memset(packet, 0, IS_PACKET_MAX_SIZE);

}

IsRelay::~IsRelay(){}

/**
* Close connection to InSim
*/
int IsRelay::isclose()
{
    #ifdef _WINDOWS
    closesocket(sock);
    WSACleanup();
    #elif defined __linux__
    close(sock);
    #endif
    return 0;
}

/**
* Get next packet ready
* This function also keeps the connection alive as long as you keep calling it
*/
int IsRelay::next_packet()
{

    char *buf = (char*)packet;

    while (true)
    {
        int retval = recv(sock, buf, 1, 0);
        if (retval <= 0)
            return -1;

           // std::cout << "Packet Size = " << (int)*buf << std::endl;
            packet_size = (int)*buf;

        retval = recv(sock, buf+1, (*buf)-1, 0);
        if (retval <= 0)
            return -1;

           // std::cout << "Get all packet" << std::endl;


        if ((peek_packet() == ISP_TINY) && (*(packet+2) == TINY_NONE))
        {
            //std::cout << "TINY_NONE" << std::endl;

            struct IS_TINY keepalive;
            keepalive.Size = sizeof(struct IS_TINY);
            keepalive.Type = ISP_TINY;
            keepalive.ReqI = 0;
            keepalive.SubT = TINY_NONE;

            // Send it back
            if (send_packet(&keepalive) < 0)
                return -1;
        }
        else
            break;
    }

    return 0;
}

/**
* Return the type of the next packet
*/
byte IsRelay::peek_packet()
{
    return *(packet+1);
}

char IsRelay::get_reqi()
{
    return *(packet+2);
}

byte IsRelay::type_packet()
{
    return *(packet+3);
}

/**
* Return the contents of the next packet
*/
void* IsRelay::get_packet()
{
    if (peek_packet() == ISP_NONE)
        return NULL;

    return packet;
}

/**
* Send a packet
*/
int IsRelay::send_packet(void* s_packet)
{
    if (send(sock, (const char *)s_packet, *((unsigned char*)s_packet), 0) < 0)
        return -1;

    return 0;
}

int IsRelay::send_packet(const char * s_packet,int s_size)
{
    if (send(sock, s_packet, s_size, 0) < 0)
        return -1;

    return 0;
}

