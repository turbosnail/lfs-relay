
#include <map>
#include <fstream>
#include "CInsim.h"
#include "IsRelay.h"

#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>     /* atoi */
#include <time.h>
#include <mysql/mysql.h>
#include <thread>


#if defined __linux__
typedef int SOCKET;
#endif // defined

#define IS_PRODUCT_NAME "LFSPRelay"

#define MY_PORT   29340

#define MYSQL_HOST "127.0.0.1"
#define MYSQL_PORT 3306
#define MYSQL_USER "lfsp_web"
#define MYSQL_PASS "FhhC2xYJU4"
#define MYSQL_DB "lfsp_web"

#ifdef _WINDOWS
#define sleep(a) Sleep(a*1000);
#endif // _WINDOWS

struct SERVER
{
    CInsim  *insim;
    int     ID;
    string	HName;	// Name of the host
    string	Track;	// Short track name
    string  Admin;
    string  Spec;
    byte	Flags;		// Info flags about the host - see NOTE 1) below
    byte	NumConns;	// Number of people on the host
    byte    NumP;
    byte    ok;
};

map<int, SERVER> servers;

/**< \brief map client socket => server id */
map<SOCKET, int> clients;


struct SERVERS
{
    char id[5];
    char IP[16];
    word Port;
    char AdminPass[25];
    char SpecPass[25];
};
