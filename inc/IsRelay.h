#ifndef _ISRELAY_H
#define _ISRELAY_H

#include "CInsim.h"
#include "relay.h"

#ifdef _WIN32
	#define _WINDOWS
#elif defined _WIN64
	#define _WINDOWS
#endif

// Includes for Windows (uses winsock2)
#ifdef _WINDOWS
#include <winsock2.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

// Includes for *NIX (no winsock2, these headers are needed instead)
#elif defined __linux__
#include <stdio.h>
#include <pthread.h>
#include <limits.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fstream>
#include <unistd.h>
#endif

#define IS_PACKET_BUFFER_SIZE 2048
#define IS_PACKET_MAX_SIZE 2048
#define IS_TIMEOUT 5

// Definition for our buffer datatype
struct isPackBuffer
{
    unsigned char buffer[IS_PACKET_BUFFER_SIZE];    // Packet buffer - 512 should be more than enough
    unsigned int bytes;                 // Number of bytes currently in buffer
};

class IsRelay
{
  private:
    unsigned char packet[IS_PACKET_MAX_SIZE];           // A buffer where the current packet is stored
    fd_set readfd, exceptfd;                // File descriptor watches

    #ifdef _WINDOWS
    struct timeval select_timeout;          // timeval struct for the select() call
    #elif defined __linux__
    struct timespec select_timeout;        // timeval struct for the pselect() call
    #endif

    struct isPackBuffer gbuf;                     // Our global buffer
    struct isPackBuffer lbuf;                     // Our local buffer


  public:
    IsRelay();                 // Constructor
    ~IsRelay();                 // Destructor
	#ifdef _WINDOWS
    SOCKET sock;                            // TCP Socket (most packets)
    #elif defined __linux__
    int sock;                               // TCP Socket (most packets)
    #endif
    byte Admin;                         // Admin on server
    int packet_size;
    int isclose();                      // Closes connection from insim and from the socket
    int next_packet();                  // Gets next packet ready into "char packet[]"
    byte peek_packet();                 // Returns the type of the current packet
    byte type_packet();                 // Returns the type of the current packet
    char get_reqi();
    void* get_packet();                 // Returns a pointer to the current packet. Must be casted
    int send_packet(void* packet);      // Sends a packet to the host
    int send_packet(const char * s_packet,int s_size);

};

#endif
